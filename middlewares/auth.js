const { Pengguna, Pesanan } = require("../models");
const jwt = require("jsonwebtoken");

const verifyToken = (req, res, next) => {
  const bearerHeader = req.headers.authorization;
  if (!bearerHeader) {
    next({ name: "MohonLogin" });
  } else {
    const token = bearerHeader.split(" ")[1];
    jwt.verify(token, process.env.JWT_KEY, (err, decoded) => {
      if (err) {
        next({ name: "TokenExpired" });
      } else {
        req.dataPengguna = { ...decoded, token };
        next();
      }
    });
  }
};

module.exports = { verifyToken };
