const errorHandler = (err, req, res, next) => {
  let statusCode, errorMessage;
  switch (err.name) {
    // register
    case "Terdaftar":
      statusCode = 409;
      errorMessage = "Email ini sudah terdaftar";
      break;

    // login
    case "BelumTerdaftar":
      statusCode = 401;
      errorMessage = "Email ini belum terdaftar";
      break;
    case "KataSandiSalah":
      statusCode = 401;
      errorMessage = "Kata sandi salah";
      break;

    // auth
    case "MohonLogin":
      statusCode = 401;
      errorMessage = "Login untuk melanjutkan";
      break;
    case "TokenExpired":
      statusCode = 401;
      errorMessage = "Token expired atau salah";
      break;
    case "BukanAdmin":
      statusCode = 403;
      errorMessage = "Masuk sebagai admin untuk menjalankan perintah ini";
      break;

    // kendaraan
    case "KendaraanTerdaftar":
      statusCode = 409;
      errorMessage = "Kendaraan dengan plat nomor ini sudah terdaftar";
      break;

    case "KendaraanTidakTersedia":
      statusCode = 403;
      errorMessage = "Kendaraan yang dipilih tidak tersedia";
      break;

    // pesanan
    case "KendaraanTidakDitemukan":
      statusCode = 404;
      errorMessage = "Kendaraan tidak ditemukan";
      break;

    // validasi
    case "Validasi":
      statusCode = 422;
      errorMessage = err.err
        .map((error) => error.message)
        .join("")
        .trim();
      break;

    case "UserTidakDitemukan":
      statusCode = 404;
      errorMessage = "Akun tidak ditemukan";
      break;

    case "DataTidakDitemukan":
      statusCode = 404;
      errorMessage = "Data tidak ditemukan";
      break;

    default:
      statusCode = 500;
      errorMessage = err;
      console.log(err);
  }
  res.status(statusCode).json({ message: errorMessage });
};

module.exports = errorHandler;
