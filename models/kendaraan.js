"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Kendaraan extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Kendaraan.hasMany(models.Pesanan);
    }
  }
  Kendaraan.init(
    {
      id: { type: DataTypes.STRING, primaryKey: true },
      nama: DataTypes.STRING,
      tipe: DataTypes.STRING,
      platNomor: DataTypes.STRING,
      warna: DataTypes.STRING,
      muatan: DataTypes.STRING,
      supportMedis: DataTypes.BOOLEAN,
      supportFestronik: DataTypes.BOOLEAN,
      supportTrucking: DataTypes.BOOLEAN,
      tersedia: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "Kendaraan",
    }
  );
  return Kendaraan;
};
