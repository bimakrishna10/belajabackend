"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Pesanan extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Pesanan.belongsTo(models.Pengguna);
      Pesanan.belongsTo(models.Kendaraan);
    }
  }
  Pesanan.init(
    {
      id: { type: DataTypes.STRING, primaryKey: true },
      PenggunaId: DataTypes.STRING,
      KendaraanId: DataTypes.STRING,
      medis: DataTypes.BOOLEAN,
      festronik: DataTypes.BOOLEAN,
      trucking: DataTypes.BOOLEAN,
      tanggalPengambilan: DataTypes.DATE,
      kodeLimbah: DataTypes.STRING,
      selesai: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "Pesanan",
    }
  );
  return Pesanan;
};
