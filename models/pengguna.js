"use strict";
const { Model } = require("sequelize");
module.exports = (sequelize, DataTypes) => {
  class Pengguna extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      Pengguna.hasMany(models.Pesanan);
    }
  }
  Pengguna.init(
    {
      id: { type: DataTypes.STRING, primaryKey: true },
      surel: DataTypes.STRING,
      kataSandi: DataTypes.STRING,
      refreshToken: DataTypes.STRING,
      namaPerusahaan: DataTypes.STRING,
      namaPJ: DataTypes.STRING,
      jabatan: DataTypes.STRING,
      nomorPonsel: DataTypes.STRING,
      admin: DataTypes.BOOLEAN,
    },
    {
      sequelize,
      modelName: "Pengguna",
    }
  );
  return Pengguna;
};
