"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addConstraint("Pesanans", {
      fields: ["KendaraanId"],
      type: "foreign key",
      name: "custom_fkey_id_kendaraan",
      references: {
        //Required field
        table: "Kendaraans",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeConstraint(
      "Pesanans",
      "custom_fkey_id_kendaraan"
    );
  },
};
