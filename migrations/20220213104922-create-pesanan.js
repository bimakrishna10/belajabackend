"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Pesanans", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      PenggunaId: {
        type: Sequelize.STRING,
      },
      KendaraanId: {
        type: Sequelize.STRING,
      },
      medis: {
        type: Sequelize.BOOLEAN,
      },
      festronik: {
        type: Sequelize.BOOLEAN,
      },
      trucking: {
        type: Sequelize.BOOLEAN,
      },
      tanggalPengambilan: {
        type: Sequelize.DATE,
      },
      kodeLimbah: {
        type: Sequelize.STRING,
      },
      selesai: {
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Pesanans");
  },
};
