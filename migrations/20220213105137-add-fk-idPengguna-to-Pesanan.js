"use strict";

module.exports = {
  async up(queryInterface, Sequelize) {
    queryInterface.addConstraint("Pesanans", {
      fields: ["PenggunaId"],
      type: "foreign key",
      name: "custom_fkey_id_pengguna",
      references: {
        //Required field
        table: "Penggunas",
        field: "id",
      },
      onDelete: "cascade",
      onUpdate: "cascade",
    });
  },

  async down(queryInterface, Sequelize) {
    await queryInterface.removeConstraint(
      "Pesanans",
      "custom_fkey_id_pengguna"
    );
  },
};
