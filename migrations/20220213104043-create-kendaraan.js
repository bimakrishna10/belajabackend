"use strict";
module.exports = {
  async up(queryInterface, Sequelize) {
    await queryInterface.createTable("Kendaraans", {
      id: {
        allowNull: false,
        primaryKey: true,
        type: Sequelize.STRING,
      },
      nama: {
        type: Sequelize.STRING,
      },
      tipe: {
        type: Sequelize.STRING,
      },
      platNomor: {
        type: Sequelize.STRING,
      },
      warna: {
        type: Sequelize.STRING,
      },
      muatan: {
        type: Sequelize.STRING,
      },
      supportMedis: {
        type: Sequelize.BOOLEAN,
      },
      supportFestronik: {
        type: Sequelize.BOOLEAN,
      },
      supportTrucking: {
        type: Sequelize.BOOLEAN,
      },
      tersedia: {
        type: Sequelize.BOOLEAN,
      },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE,
      },
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable("Kendaraans");
  },
};
