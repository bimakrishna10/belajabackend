require("dotenv").config();
const nodemailer = require("nodemailer");

let transporter = nodemailer.createTransport({
  service: "gmail",
  auth: {
    type: "OAuth2",
    user: process.env.MAIL_USERNAME,
    pass: process.env.MAIL_PASSWORD,
    clientId: process.env.OAUTH_CLIENTID,
    clientSecret: process.env.OAUTH_CLIENT_SECRET,
    refreshToken: process.env.OAUTH_REFRESH_TOKEN,
  },
});

function sendMail(to, subject, html, callback) {
  transporter.sendMail(
    {
      from: "belaja.indonesia@gmail.com",
      to,
      subject,
      html,
    },
    function (err, data) {
      if (err) {
        callback(err, null);
      } else {
        callback(null, "Email terkirim!");
      }
    }
  );
}
module.exports = sendMail;
