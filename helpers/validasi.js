const moment = require("moment");
function validasi(req, fields) {
  let err = [];
  fields.forEach((field) => {
    switch (field) {
      // Pengguna
      case "surel":
        const { surel } = req.body;
        if (surel.trim?.() == "") {
          err.push({ name: "surel", message: emptyMessage("surel") });
        } else if (
          !surel
            .toLowerCase()
            .match(
              /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )
        ) {
          err.push({ name: "surel", message: invalidMessage("surel") });
        }
        break;
      case "kataSandi":
        const { kataSandi } = req.body;
        if (kataSandi.trim?.() == "") {
          err.push({ name: "kataSandi", message: emptyMessage("kataSandi") });
        }
        break;
      case "admin":
        const { admin } = req.body;
        if (admin.trim?.() !== "") {
          if (
            admin != "true" &&
            admin != "false" &&
            typeof admin != "boolean"
          ) {
            err.push({
              name: "admin",
              message: invalidMessage("admin"),
            });
          }
        }
        break;

      // Kendaraan
      case "nama":
        const { nama } = req.body;
        if (nama.trim?.() == "") {
          err.push({ name: "nama", message: emptyMessage("nama") });
        }
        break;
      case "tipe":
        const { tipe } = req.body;
        if (tipe.trim?.() == "") {
          err.push({ name: "tipe", message: emptyMessage("tipe") });
        }
        break;
      case "platNomor":
        const { platNomor } = req.body;
        if (platNomor.trim?.() == "") {
          err.push({ name: "platNomor", message: emptyMessage("platNomor") });
        }
        break;
      case "muatan":
        const { muatan } = req.body;
        if (muatan.trim?.() == "") {
          err.push({ name: "muatan", message: emptyMessage("muatan") });
        } else if (isNaN(Number(muatan))) {
          err.push({ name: "muatan", message: invalidMessage("muatan") });
        }
        break;
      case "supportMedis":
        const { supportMedis } = req.body;
        if (supportMedis.trim?.() == "") {
          err.push({
            name: "supportMedis",
            message: emptyMessage("supportMedis"),
          });
        } else if (
          supportMedis != "true" &&
          supportMedis != "false" &&
          typeof supportMedis != "boolean"
        ) {
          err.push({
            name: "supportMedis",
            message: invalidMessage("supportMedis"),
          });
        }
        break;
      case "supportFestronik":
        const { supportFestronik } = req.body;
        if (supportFestronik.trim?.() == "") {
          err.push({
            name: "supportFestronik",
            message: emptyMessage("supportFestronik"),
          });
        } else if (
          supportFestronik != "true" &&
          supportFestronik != "false" &&
          typeof supportFestronik != "boolean"
        ) {
          err.push({
            name: "supportFestronik",
            message: invalidMessage("supportFestronik"),
          });
        }
        break;
      case "supportTrucking":
        const { supportTrucking } = req.body;
        if (supportTrucking.trim?.() == "") {
          err.push({
            name: "supportTrucking",
            message: emptyMessage("supportTrucking"),
          });
        } else if (
          supportTrucking != "true" &&
          supportTrucking != "false" &&
          typeof supportTrucking != "boolean"
        ) {
          err.push({
            name: "supportTrucking",
            message: invalidMessage("supportTrucking"),
          });
        }
        break;

      // Pesanan
      case "medis":
        const { medis } = req.body;
        console.log("medis", medis);
        if (medis == undefined || medis.trim?.() == "") {
          err.push({
            name: "medis",
            message: emptyMessage("medis"),
          });
        } else if (
          medis != "true" &&
          medis != "false" &&
          typeof medis != "boolean"
        ) {
          err.push({
            name: "medis",
            message: invalidMessage("medis"),
          });
        }
        break;
      case "festronik":
        const { festronik } = req.body;
        if (festronik == undefined || festronik.trim?.() == "") {
          err.push({
            name: "festronik",
            message: emptyMessage("festronik"),
          });
        } else if (
          festronik != "true" &&
          festronik != "false" &&
          typeof festronik != "boolean"
        ) {
          err.push({
            name: "festronik",
            message: invalidMessage("festronik"),
          });
        }
        break;
      case "trucking":
        const { trucking } = req.body;
        if (trucking == undefined || trucking.trim?.() == "") {
          err.push({
            name: "trucking",
            message: emptyMessage("trucking"),
          });
        } else if (
          trucking != "true" &&
          trucking != "false" &&
          typeof trucking != "boolean"
        ) {
          err.push({
            name: "trucking",
            message: invalidMessage("trucking"),
          });
        }
        break;
      case "tanggalPengambilan":
        const { tanggalPengambilan } = req.body;
        if (
          tanggalPengambilan == undefined ||
          tanggalPengambilan.trim?.() == ""
        ) {
          err.push({
            name: "tanggalPengambilan",
            message: emptyMessage("tanggalPengambilan"),
          });
        } else if (!moment(new Date(tanggalPengambilan)).isValid()) {
          err.push({
            name: "tanggalPengambilan",
            message: invalidMessage("tanggalPengambilan"),
          });
        }
        break;
      case "kodeLimbah":
        const { kodeLimbah } = req.body;
        if (kodeLimbah == undefined || kodeLimbah.trim?.() == "") {
          err.push({ name: "kodeLimbah", message: emptyMessage("kodeLimbah") });
        }
        break;
      case "selesai":
        const { selesai } = req.body;
        if (selesai == undefined || selesai.trim?.() == "") {
          err.push({
            name: "selesai",
            message: emptyMessage("selesai"),
          });
        } else if (
          selesai != "true" &&
          selesai != "false" &&
          typeof selesai != "boolean"
        ) {
          err.push({
            name: "selesai",
            message: invalidMessage("selesai"),
          });
        }
        break;

      // mail
      case "tujuan":
        const { tujuan } = req.body;
        if (tujuan.trim?.() == "") {
          err.push({ name: "tujuan", message: emptyMessage("tujuan") });
        } else if (
          !tujuan
            .toLowerCase()
            .match(
              /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
            )
        ) {
          err.push({ name: "tujuan", message: invalidMessage("tujuan") });
        }
        break;
      case "judul":
        const { judul } = req.body;
        if (judul == undefined || judul.trim?.() == "") {
          err.push({ name: "judul", message: emptyMessage("judul") });
        }
        break;
      case "html":
        const { html } = req.body;
        if (html == undefined || html.trim?.() == "") {
          err.push({ name: "html", message: emptyMessage("html") });
        }
        break;
    }
  });
  return err;
}

const emptyMessage = (fieldName) => {
  return `Kolom ${fieldName} dibutuhkan. `;
};

const invalidMessage = (fieldName) => {
  return `Format kolom ${fieldName} salah. `;
};

module.exports = validasi;
