const kendaraanRouter = require("express").Router();
const Controller = require("../controllers/kendaraanController");

kendaraanRouter.get("/", Controller.listKendaraan);
kendaraanRouter.post("/", Controller.filterKendaraan);
kendaraanRouter.get("/:id", Controller.listBerdasarkanId);
kendaraanRouter.post("/add", Controller.tambahKendaraan);
kendaraanRouter.put("/:id", Controller.ubahKendaraan);
kendaraanRouter.delete("/:id", Controller.hapusKendaraan);

module.exports = kendaraanRouter;
