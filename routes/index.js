const router = require("express").Router();
const penggunaRoutes = require("./penggunaRoutes");
const kendaraanRoutes = require("./kendaraanRoutes");
const pesananRoutes = require("./pesananRoutes");
const adminRoutes = require("./adminRoutes");
const MailController = require("../controllers/mailController");

router.post("/sendMail", MailController.sendMail);

router.use("/vehicles", kendaraanRoutes);
router.use("/", penggunaRoutes);
router.use("/orders", pesananRoutes);
router.use("/admin", adminRoutes);

module.exports = router;
