const adminRoutes = require("express").Router();
const Controller = require("../controllers/adminController");

adminRoutes.post("/getUsers", Controller.getUsers);
adminRoutes.post("/getVehicles", Controller.getVehicles);
adminRoutes.post("/getOrders", Controller.getOrders);
adminRoutes.post("/changeUserPassword", Controller.changeUserPassword);
adminRoutes.put("/editUser", Controller.editUser);
adminRoutes.delete("/deleteUser/:id", Controller.deleteUser);
adminRoutes.put("/editOrder", Controller.editOrder);

module.exports = adminRoutes;
