const penggunaRouter = require("express").Router();
const Controller = require("../controllers/penggunaController");
const { verifyToken } = require("../middlewares/auth");

penggunaRouter.get("/", (req, res) => {
  res.json({ message: "Test" });
});

penggunaRouter.post("/login", Controller.masuk);
penggunaRouter.post("/register", Controller.daftar);

penggunaRouter.use(verifyToken);
penggunaRouter.get("/refreshToken", Controller.refreshToken);
penggunaRouter.get("/account", Controller.getInformasiPengguna);
penggunaRouter.put("/account", Controller.ubahInformasiPengguna);
penggunaRouter.post("/changePassword", Controller.ubahPassword);

module.exports = penggunaRouter;
