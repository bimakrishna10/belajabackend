const pesananRouter = require("express").Router();
const Controller = require("../controllers/pesananController");

pesananRouter.get("/", Controller.listPesanan);
pesananRouter.post("/", Controller.filterPesanan);
pesananRouter.get("/:id", Controller.cariBerdasarkanId);
pesananRouter.post("/add", Controller.tambahPesanan);
pesananRouter.put("/:id", Controller.ubahPesanan);
pesananRouter.delete("/:id", Controller.hapusPesanan);

module.exports = pesananRouter;
