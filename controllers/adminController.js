const { Pengguna, Pesanan, Kendaraan } = require("../models");
const validasi = require("../helpers/validasi");
const Sequelize = require("sequelize");
const { gte, lte, like } = Sequelize.Op;
const bcrypt = require("bcrypt");

class Controller {
  static isAdmin(id) {
    return Pengguna.findOne({ where: { id } })
      .then((data) => {
        if (data.dataValues.admin) {
          return true;
        } else {
          return false;
        }
      })
      .catch(() => false);
  }

  static async getUsers(req, res, next) {
    const { id } = req.dataPengguna;
    const isAdmin = await Controller.isAdmin(id);

    if (!isAdmin) {
      next({ name: "BukanAdmin" });
    } else {
      let options = {};
      for (const key in req.body) {
        if (key == "admin") {
          options[key] = req.body[key];
        } else if (req.body[key] && typeof req.body[key] == "string") {
          options[key] = { [like]: `%${req.body[key]}%` };
        }
      }

      Pengguna.findAll({ where: options })
        .then((data) => {
          const payload = data.map((user) => {
            const { kataSandi, refreshToken, ...rest } = user.dataValues;
            return rest;
          });
          res.status(200).json({ message: "Sukses!", payload });
        })
        .catch((err) => next(err));
    }
  }

  static async getVehicles(req, res, next) {
    const { id } = req.dataPengguna;
    const isAdmin = await Controller.isAdmin(id);

    if (!isAdmin) {
      next({ name: "BukanAdmin" });
    } else {
      let options = {};
      for (const key in req.body) {
        if (req.body[key] && key == "minMuatan") {
          options.muatan = { [gte]: req.body.minMuatan };
        } else if (
          req.body[key] &&
          (key == "tersedia" ||
            key == "supportMedis" ||
            key == "supportFestronik" ||
            key == "supportTrucking")
        ) {
          options[key] = req.body[key];
        } else if (req.body[key] && typeof req.body[key] == "string") {
          options[key] = { [like]: `%${req.body[key]}%` };
        }
      }

      Kendaraan.findAll({ where: options })
        .then((data) => {
          res.status(200).json({ message: "Sukses!", payload: data });
        })
        .catch((err) => next(err));
    }
  }

  static async getOrders(req, res, next) {
    const { id } = req.dataPengguna;
    const isAdmin = await Controller.isAdmin(id);

    if (!isAdmin) {
      next({ name: "BukanAdmin" });
    } else {
      let options = {};
      for (const key in req.body) {
        if (req.body[key] && key == "mulaiTanggalPengambilan") {
          options.tanggalPengambilan[gte] = req.body.mulaiTanggalPengambilan;
        } else if (req.body[key] && key == "akhirTanggalPengambilan") {
          options.tanggalPengambilan[lte] = req.body.akhirTanggalPengambilan;
        } else if (req.body[key] && key == "kodeLimbah") {
          options[key] = { [like]: `%${req.body[key]}%` };
        } else if (req.body[key] != null) {
          options[key] = req.body[key];
        }
      }
      Pesanan.findAll({ where: options, include: [Pengguna, Kendaraan] })
        .then((data) => {
          const payload = data.map((user) => {
            const { Pengguna, ...rest } = user.dataValues;
            const { kataSandi, refreshToken, ...restPengguna } =
              Pengguna.dataValues;
            return { ...rest, Pengguna: restPengguna };
          });
          res.status(200).json({ message: "Sukses!", payload });
        })
        .catch((err) => next(err));
    }
  }

  static async editOrder(req, res, next) {
    const { id } = req.dataPengguna;
    const isAdmin = await Controller.isAdmin(id);

    if (!isAdmin) {
      next({ name: "BukanAdmin" });
    } else {
      Pesanan.findOne({ where: { id: req.body.PesananId } })
        .then((data) => {
          if (!data) {
            throw { name: "DataTidakDitemukan" };
          } else {
            let validasiArr = [];
            for (const key in req.body) {
              if (req.body[key] != null) {
                validasiArr.push(key);
              }
            }
            let err = validasi(req, validasiArr);

            if (err && err.length > 0) {
              next({ name: "Validasi", err });
            } else {
              return Pesanan.update(
                {
                  KendaraanId: req.body.KendaraanId || data.KendaraanId,
                  medis: req.body.medis != null ? req.body.medis : data.medis,
                  festronik:
                    req.body.festronik != null
                      ? req.body.festronik
                      : data.festronik,
                  trucking:
                    req.body.trucking != null
                      ? req.body.trucking
                      : data.trucking,
                  tanggalPengambilan:
                    req.body.tanggalPengambilan || data.tanggalPengambilan,
                  kodeLimbah: req.body.kodeLimbah || data.kodeLimbah,
                  selesai:
                    req.body.selesai != null ? req.body.selesai : data.selesai,
                },
                {
                  where: { id: req.body.PesananId },
                  returning: true,
                  plain: true,
                }
              );
            }
          }
        })
        .then((data) => {
          res
            .status(200)
            .json({ message: "Data berhasil diubah!", payload: data[1] });
        })
        .catch((err) => next(err));
    }
  }

  static async changeUserPassword(req, res, next) {
    const { id } = req.dataPengguna;
    const isAdmin = await Controller.isAdmin(id);

    let err = validasi(req, ["surel", "kataSandi"]);

    if (err && err.length > 0) {
      next({ name: "Validasi", err });
    } else {
      if (!isAdmin) {
        next({ name: "BukanAdmin" });
      } else {
        Pengguna.findOne({ where: { surel: req.body.surel } })
          .then((data) => {
            if (!data) {
              throw { name: "UserTidakDitemukan" };
            } else {
              const salt = bcrypt.genSaltSync(10);
              const hashed = bcrypt.hashSync(req.body.kataSandi, salt);

              return Pengguna.update(
                { kataSandi: hashed },
                {
                  where: { surel: req.body.surel },
                  returning: true,
                  plain: true,
                }
              );
            }
          })
          .then(() => {
            res.status(200).json({ message: "Kata sandi berhasil diubah!" });
          })
          .catch((err) => next(err));
      }
    }
  }

  static async editUser(req, res, next) {
    console.log("body", req.body, "admin", req.dataPengguna);
    const { id } = req.dataPengguna;
    const isAdmin = await Controller.isAdmin(id);

    let err = validasi(req, ["PenggunaId"]);

    if (err && err.length > 0) {
      next({ name: "Validasi", err });
    } else {
      if (!isAdmin) {
        next({ name: "BukanAdmin" });
      } else {
        Pengguna.findOne({ where: { id: req.body.PenggunaId } })
          .then((data) => {
            if (!data) {
              throw { name: "DataTidakDitemukan" };
            } else {
              return Pengguna.update(
                {
                  surel: req.body.surel || data.surel,
                  namaPerusahaan:
                    req.body.namaPerusahaan || data.namaPerusahaan,
                  namaPJ: req.body.namaPJ || data.namaPJ,
                  jabatan: req.body.jabatan || data.jabatan,
                  nomorPonsel: req.body.nomorPonsel || data.nomorPonsel,
                  admin: req.body.admin != null ? req.body.admin : data.admin,
                },
                {
                  where: { id: req.body.PenggunaId },
                  returning: true,
                  plain: true,
                }
              );
            }
          })
          .then((data) => {
            const { kataSandi, refreshToken, ...payload } = data[1].dataValues;
            res.status(200).json({ message: "Data berhasil diubah!", payload });
          })
          .catch((err) => next(err));
      }
    }
  }

  static async deleteUser(req, res, next) {
    const { id: PenggunaId } = req.params;
    const { id } = req.dataPengguna;
    const isAdmin = await Controller.isAdmin(id);

    if (!isAdmin) {
      next({ name: "BukanAdmin" });
    } else {
      Pengguna.findOne({ where: { id: PenggunaId } })
        .then((data) => {
          if (!data) {
            throw { name: "DataTidakDitemukan" };
          } else {
            return Pengguna.destroy({ where: { id: PenggunaId } });
          }
        })
        .then(() => res.status(200).json({ message: "Data berhasil dihapus!" }))
        .catch((err) => next(err));
    }
  }
}

module.exports = Controller;
