const { Pengguna, Pesanan, Kendaraan } = require("../models");
const { v4: uuidv4 } = require("uuid");
const validasi = require("../helpers/validasi");
const Sequelize = require("sequelize");
const { gte } = Sequelize.Op;

class Controller {
  static listKendaraan(req, res, next) {
    Kendaraan.findAll({
      order: [["tipe", "ASC"]],
    })
      .then((data) =>
        res.status(200).json({ message: "Sukses!", payload: data })
      )
      .catch((err) => next(err));
  }

  static listBerdasarkanId(req, res, next) {
    Kendaraan.findOne({ where: { id: req.params.id } })
      .then((data) => {
        if (!data) {
          throw { name: "DataTidakDitemukan" };
        } else {
          return res.status(200).json({ message: "Sukses!", payload: data });
        }
      })
      .catch((err) => next(err));
  }

  static filterKendaraan(req, res, next) {
    let options = {};
    for (const key in req.body) {
      if (req.body[key] && key == "minMuatan") {
        options.muatan = { [gte]: req.body.minMuatan };
      } else if (req.body[key] != null) {
        options[key] = req.body[key];
      }
    }
    console.log("body >>", req.body);
    console.log("options >>", options);
    Kendaraan.findAll({
      where: options,
    })
      .then((data) =>
        res.status(200).json({ message: "Sukses!", payload: data })
      )
      .catch((err) => next(err));
  }

  static tambahKendaraan(req, res, next) {
    let err = validasi(req, [
      "nama",
      "tipe",
      "platNomor",
      "muatan",
      "supportMedis",
      "supportFestronik",
      "supportTrucking",
    ]);

    if (err && err.length > 0) {
      next({ name: "Validasi", err });
    } else {
      let { platNomor } = req.body;
      platNomor = platNomor.split(" ").join("");
      Kendaraan.findOne({ where: { platNomor } })
        .then((data) => {
          if (data) {
            throw { name: "KendaraanTerdaftar" };
          } else {
            return Kendaraan.create({
              id: uuidv4(),
              nama: req.body.nama,
              tipe: req.body.tipe,
              platNomor: platNomor,
              warna: req.body.warna,
              muatan: req.body.muatan,
              supportMedis: req.body.supportMedis,
              supportFestronik: req.body.supportFestronik,
              supportTrucking: req.body.supportTrucking,
              tersedia: true,
            });
          }
        })
        .then((data) => {
          res
            .status(201)
            .json({ message: "Data berhasil ditambahkan!", payload: data });
        })
        .catch((err) => next(err));
    }
  }

  static ubahKendaraan(req, res, next) {
    Kendaraan.findOne({ where: { id: req.params.id } })
      .then((data) => {
        if (!data) {
          throw { name: "DataTidakDitemukan" };
        } else {
          let validasiArr = [];
          for (const key in req.body) {
            if (req.body[key] != null) {
              validasiArr.push(key);
            }
          }
          let err = validasi(req, validasiArr);

          if (err && err.length > 0) {
            next({ name: "Validasi", err });
          } else {
            return Kendaraan.update(
              {
                nama: req.body.nama || data.nama,
                tipe: req.body.tipe || data.tipe,
                platNomor: req.body.platNomor || data.platNomor,
                warna: req.body.warna || data.warna,
                muatan: req.body.muatan || data.muatan,
                supportMedis:
                  req.body.supportMedis != null
                    ? req.body.supportMedis
                    : data.supportMedis,
                supportFestronik:
                  req.body.supportFestronik != null
                    ? req.body.supportFestronik
                    : data.supportFestronik,
                supportTrucking:
                  req.body.supportTrucking != null
                    ? req.body.supportTrucking
                    : data.supportTrucking,
                tersedia:
                  req.body.tersedia != null ? req.body.tersedia : data.tersedia,
              },
              { where: { id: req.params.id }, returning: true, plain: true }
            );
          }
        }
      })
      .then((data) => {
        res
          .status(200)
          .json({ message: "Data berhasil diubah!", payload: data[1] });
      })
      .catch((err) => next(err));
  }

  static hapusKendaraan(req, res, next) {
    Kendaraan.findOne({ where: { id: req.params.id } })
      .then((data) => {
        if (!data) {
          throw { name: "DataTidakDitemukan" };
        } else {
          return Kendaraan.destroy({ where: { id: req.params.id } });
        }
      })
      .then(() => res.status(200).json({ message: "Data berhasil dihapus!" }))
      .catch((err) => next(err));
  }
}

module.exports = Controller;
