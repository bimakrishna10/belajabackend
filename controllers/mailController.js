const validasi = require("../helpers/validasi");
const nodemailer = require("../helpers/nodemailer");

class Controller {
  static sendMail(req, res, next) {
    let err = validasi(req, ["tujuan", "judul", "html"]);

    if (err && err.length > 0) {
      next({ name: "Validasi", err });
    } else {
      nodemailer(
        req.body.tujuan,
        req.body.judul,
        req.body.html,
        (err, data) => {
          if (err) {
            next(err);
          } else {
            res.status(200).json({ message: data });
          }
        }
      );
    }
  }
}

module.exports = Controller;
