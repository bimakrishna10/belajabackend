const { Pengguna, Pesanan, Kendaraan } = require("../models");
const { v4: uuidv4 } = require("uuid");
const validasi = require("../helpers/validasi");

class Controller {
  static listPesanan(req, res, next) {
    const { id } = req.dataPengguna;
    Pesanan.findAll({
      where: { PenggunaId: id },
      include: [Kendaraan],
      order: [["tanggalPengambilan", "DESC"]],
    })
      .then((data) => {
        res.status(200).json({ message: "Sukses!", payload: data });
      })
      .catch((err) => next(err));
  }

  static filterPesanan(req, res, next) {
    const { id } = req.dataPengguna;
    let options = {};
    for (const key in req.body) {
      if (req.body[key] != null) {
        options[key] = req.body[key];
      }
    }

    Pesanan.findAll({
      where: { PenggunaId: id, ...options },
      include: [Kendaraan],
      order: [["tanggalPengambilan", "DESC"]],
    })
      .then((data) => {
        res.status(200).json({ message: "Sukses!", payload: data });
      })
      .catch((err) => next(err));
  }

  static cariBerdasarkanId(req, res, next) {
    const { id } = req.dataPengguna;
    Pesanan.findOne({ where: { id: req.params.id }, include: [Kendaraan] })
      .then((data) => {
        if (!data) {
          throw { name: "DataTidakDitemukan" };
        } else if (data.PenggunaId != id) {
          throw { name: "TidakTerotorisasi" };
        } else {
          res.status(200).json({ message: "Sukses!", payload: data });
        }
      })
      .catch((err) => next(err));
  }

  static tambahPesanan(req, res, next) {
    const { id } = req.dataPengguna;
    let dataKendaraan;
    let err = validasi(req, [
      "medis",
      "festronik",
      "trucking",
      "tanggalPengambilan",
      "kodeLimbah",
    ]);

    if (err && err.length > 0) {
      throw { name: "Validasi", err };
    } else {
      Kendaraan.findOne({ where: { id: req.body.KendaraanId } })
        .then((data) => {
          if (!data) {
            throw { name: "KendaraanTidakDitemukan" };
          } else if (!data.dataValues.tersedia) {
            throw { name: "KendaraanTidakTersedia" };
          } else {
            dataKendaraan = data.dataValues;
            return Kendaraan.update(
              { tersedia: false },
              {
                where: { id: req.body.KendaraanId },
                returning: true,
                plain: true,
              }
            );
          }
        })
        .then(() => {
          return Pesanan.create({
            id: uuidv4(),
            PenggunaId: id,
            KendaraanId: req.body.KendaraanId,
            medis: req.body.medis,
            festronik: req.body.festronik,
            trucking: req.body.trucking,
            tanggalPengambilan: req.body.tanggalPengambilan,
            kodeLimbah: req.body.kodeLimbah,
            selesai: false,
          });
        })
        .then((data) => {
          const { dataValues } = data;
          console.log("dataV", dataValues);
          res.status(201).json({
            message: "Pesanan berhasil dibuat",
            payload: { ...dataValues, Kendaraan: dataKendaraan },
          });
        })
        .catch((err) => next(err));
    }
  }

  static ubahPesanan(req, res, next) {
    const { id } = req.dataPengguna;
    Pesanan.findOne({ where: { id: req.params.id } })
      .then((data) => {
        if (!data) {
          throw { name: "DataTidakDitemukan" };
        } else if (data.PenggunaId != id) {
          throw { name: "TidakTerotorisasi" };
        } else {
          let validasiArr = [];
          for (const key in req.body) {
            if (req.body[key] != null) {
              validasiArr.push(key);
            }
          }
          let err = validasi(req, validasiArr);

          if (err && err.length > 0) {
            next({ name: "Validasi", err });
          } else {
            return Pesanan.update(
              {
                KendaraanId: req.body.KendaraanId || data.KendaraanId,
                medis: req.body.medis != null ? req.body.medis : data.medis,
                festronik:
                  req.body.festronik != null
                    ? req.body.festronik
                    : data.festronik,
                trucking:
                  req.body.trucking != null ? req.body.trucking : data.trucking,
                tanggalPengambilan:
                  req.body.tanggalPengambilan || data.tanggalPengambilan,
                kodeLimbah: req.body.kodeLimbah || data.kodeLimbah,
                selesai:
                  req.body.selesai != null ? req.body.selesai : data.selesai,
              },
              { where: { id: req.params.id }, returning: true, plain: true }
            );
          }
        }
      })
      .then((data) => {
        res
          .status(200)
          .json({ message: "Data berhasil diubah!", payload: data[1] });
      })
      .catch((err) => next(err));
  }

  static hapusPesanan(req, res, next) {
    const { id } = req.dataPengguna;
    Pesanan.findOne({ where: { id: req.params.id } })
      .then((data) => {
        if (!data) {
          throw { name: "DataTidakDitemukan" };
        } else if (data.PenggunaId != id) {
          throw { name: "TidakTerotorisasi" };
        } else {
          return Pesanan.destroy({ where: { id: req.params.id } });
        }
      })
      .then(() => res.status(200).json({ message: "Data berhasil dihapus!" }))
      .catch((err) => next(err));
  }
}

module.exports = Controller;
