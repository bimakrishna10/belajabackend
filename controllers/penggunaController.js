const { Pengguna, Pesanan, Kendaraan } = require("../models");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { v4: uuidv4 } = require("uuid");
const validasi = require("../helpers/validasi");

class Controller {
  static daftar(req, res, next) {
    let err = validasi(req, ["surel", "kataSandi", "admin"]);

    if (err && err.length > 0) {
      next({ name: "Validasi", err });
    } else {
      Pengguna.findOne({ where: { surel: req.body.surel } })
        .then((data) => {
          if (data) {
            throw { name: "Terdaftar" };
          } else {
            const salt = bcrypt.genSaltSync(10);
            const hashed = bcrypt.hashSync(req.body.kataSandi, salt);
            return Pengguna.create({
              id: uuidv4(),
              surel: req.body.surel,
              kataSandi: hashed,
              namaPerusahaan: req.body.namaPerusahaan || null,
              namaPJ: req.body.namaPJ || null,
              jabatan: req.body.jabatan || null,
              nomorPonsel: req.body.nomorPonsel || null,
              admin: req.body.admin || false,
            });
          }
        })
        .then(() => {
          res.status(201).json({
            message: "Pendaftaran sukses!",
          });
        })
        .catch((err) => next(err));
    }
  }

  static masuk(req, res, next) {
    let err = validasi(req, ["surel", "kataSandi"]);

    if (err && err.length > 0) {
      next({ name: "Validasi", err });
    } else {
      Pengguna.findOne({ where: { surel: req.body.surel } })
        .then((data) => {
          if (!data) {
            throw { name: "BelumTerdaftar" };
          } else if (!bcrypt.compareSync(req.body.kataSandi, data.kataSandi)) {
            throw { name: "KataSandiSalah" };
          } else {
            const token = jwt.sign(
              {
                id: data.id,
                surel: data.surel,
              },
              process.env.JWT_KEY,
              { expiresIn: 3600 }
            );
            const refreshToken = jwt.sign({ id: data.id }, process.env.JWT_KEY);
            Controller.setRefreshToken(data.id, refreshToken);
            return res.status(200).json({
              message: "Login Sukses",
              payload: { surel: data.surel, token, refreshToken },
            });
          }
        })
        .catch((err) => next(err));
    }
  }

  static refreshToken(req, res, next) {
    const { id, token } = req.dataPengguna;
    Pengguna.findOne({ where: { id } })
      .then((data) => {
        if (data.refreshToken != token) {
          throw { name: "TokenExpired" };
        } else {
          const token = jwt.sign(
            {
              id: data.id,
              surel: data.surel,
            },
            process.env.JWT_KEY,
            { expiresIn: 3600 }
          );
          const refreshToken = jwt.sign({ id: data.id }, process.env.JWT_KEY);
          Controller.setRefreshToken(data.id, refreshToken);
          return res.status(200).json({
            message: "Refresh Token Sukses",
            payload: { surel: data.surel, token, refreshToken },
          });
        }
      })
      .catch((err) => next(err));
  }

  static setRefreshToken(PenggunaId, refreshToken) {
    Pengguna.update(
      {
        refreshToken,
      },
      { where: { id: PenggunaId }, plain: true }
    );
  }

  static getInformasiPengguna(req, res, next) {
    const { id } = req.dataPengguna;
    Pengguna.findOne({ where: { id } })
      .then((data) => {
        if (!data) {
          throw { name: "IdTidakDitemukan" };
        } else {
          const { kataSandi, refreshToken, ...payload } = data.dataValues;
          res.status(200).json({
            message: "Sukses!",
            payload,
          });
        }
      })
      .catch((err) => next(err));
  }

  static ubahInformasiPengguna(req, res, next) {
    const { id } = req.dataPengguna;
    Pengguna.findOne({ where: { id } })
      .then((data) => {
        if (!data) {
          throw { name: "IdTidakDitemukan" };
        } else {
          let validasiArr = [];
          for (const key in req.body) {
            if (req.body[key] != null) {
              validasiArr.push(key);
            }
          }
          let err = validasi(req, validasiArr);

          if (err && err.length > 0) {
            next({ name: "Validasi", err });
          } else {
            return Pengguna.findOne({ where: { surel: req.body.surel } });
          }
        }
      })
      .then((data) => {
        if (data && data.id != id) {
          throw { name: "Terdaftar" };
        } else {
          return Pengguna.update(
            {
              surel: req.body.surel || data.surel,
              namaPerusahaan: req.body.namaPerusahaan || data.namaPerusahaan,
              namaPJ: req.body.namaPJ || data.namaPJ,
              jabatan: req.body.jabatan || data.jabatan,
              nomorPonsel: req.body.nomorPonsel || data.nomorPonsel,
              admin: req.body.admin != null ? req.body.admin : data.admin,
            },
            { where: { id }, returning: true, plain: true }
          );
        }
      })
      .then((data) => {
        const { kataSandi, refreshToken, ...payload } = data[1].dataValues;
        res.status(200).json({ message: "Data berhasil diubah!", payload });
      })
      .catch((err) => next(err));
  }

  static ubahPassword(req, res, next) {
    const { id } = req.dataPengguna;
    Pengguna.findOne({ where: { id } })
      .then((data) => {
        if (!data) {
          throw { name: "IdTidakDitemukan" };
        } else if (
          !bcrypt.compareSync(req.body.kataSandiLama, data.kataSandi)
        ) {
          throw { name: "KataSandiSalah" };
        } else {
          const salt = bcrypt.genSaltSync(10);
          const hashed = bcrypt.hashSync(req.body.kataSandiBaru, salt);
          return Pengguna.update({ kataSandi: hashed }, { where: { id } });
        }
      })
      .then(() => {
        res.status(200).json({ message: "Kata sandi berhasil diubah!" });
      })
      .catch((err) => next(err));
  }
}

module.exports = Controller;
